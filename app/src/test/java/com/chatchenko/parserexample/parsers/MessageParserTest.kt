package com.chatchenko.parserexample.parsers

import com.chatchenko.parserexample.network.TitleFetcher
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MessageParserTest {
    private lateinit var parser: MessageParser
    @Mock private lateinit var mockTitleFetcher: TitleFetcher

    @Before fun setUp() {
        MockitoAnnotations.initMocks(this)
        parser = MessageParser(mockTitleFetcher)
    }

    @Test fun shouldFindAllElements() {
        val url = "https://twitter.com/jdorfman/status/430511497475670016"
        val message = "@bob @john (success) such a cool feature; $url"
        val expectedTitle = "page title"
        Mockito.`when`(mockTitleFetcher.fetchPageTitle(url)).thenReturn(Single.just(expectedTitle))

        val expected = ParsedMessage(
                mentions = listOf("bob", "john"),
                emoticons = listOf("success"),
                links = listOf(UrlDescription(url, expectedTitle))
        )
        parser.parse(message).test()
                .assertValue(expected)
                .assertComplete()
    }
}