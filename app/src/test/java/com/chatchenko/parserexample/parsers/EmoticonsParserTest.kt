package com.chatchenko.parserexample.parsers

import org.junit.Before
import org.junit.Test

class EmoticonsParserTest {
    private lateinit var parser: Parser<List<String>>

    @Before fun setUp() {
        parser = Parser.emoticons()
    }

    @Test fun shouldParseSingleEmoticons() {
        parser.parse("Good morning! (megusta)").test()
                .assertValue(listOf("megusta"))
                .assertComplete()
    }

    @Test fun shouldParseAlphaNumeric() {
        parser.parse("Good morning! (megusta123)").test()
                .assertValue(listOf("megusta123"))
                .assertComplete()
    }

    @Test fun shouldIgnoreNonAlphaNumeric() {
        parser.parse("Good morning! (meg_usta)").test()
                .assertValue(emptyList<String>())
                .assertComplete()
    }

    @Test fun shouldParseMaxSize() {
        parser.parse("Good morning! (123456789012345)").test()
                .assertValue(listOf("123456789012345"))
                .assertComplete()
    }

    @Test fun shouldIgnoreIfMaxSizeExceeded() {
        parser.parse("Good morning! (1234567890123456)").test()
                .assertValue(emptyList<String>())
                .assertComplete()
    }
}