package com.chatchenko.parserexample.parsers

import org.junit.Before
import org.junit.Test

class MentionsParserTest {
    private lateinit var parser: Parser<List<String>>

    @Before fun setUp() {
        parser = Parser.mentions()
    }

    @Test fun shouldParseMentionInTheBeginning() {
        parser.parse("@chris you around?").test()
                .assertValue(listOf("chris"))
                .assertComplete()
    }

    @Test fun shouldParseInTheMiddle() {
        parser.parse("@chris @mydogtom you around?").test()
                .assertValue(listOf("chris", "mydogtom"))
                .assertComplete()
    }

    @Test fun shouldIgnoreEmail() {
        parser.parse("this is my mail example@mail.com").test()
                .assertValue(emptyList<String>())
                .assertComplete()
    }

    @Test fun shouldParseWithNumbers() {
        parser.parse("@chris123 you around?").test()
                .assertValue(listOf("chris123"))
                .assertComplete()
    }

    @Test fun shouldIgnoreNonAlphaNumericSymbols() {
        parser.parse("@chris @strange_name you around?").test()
                .assertValue(listOf("chris", "strange"))
                .assertComplete()
    }

    @Test fun shouldParseOnNewLine() {
        parser.parse("@chris\n@bob").test()
                .assertValue(listOf("chris", "bob"))
    }
}