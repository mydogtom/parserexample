package com.chatchenko.parserexample.parsers

import com.chatchenko.parserexample.network.TitleFetcher
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(Enclosed::class)
class WebLinkParserTest {

    @RunWith(Parameterized::class)
    class DifferentLinksTest(private val link: String, private val pageTitle: String) {

        companion object {
            @JvmStatic
            @Parameterized.Parameters(name = "should find {0}")
            fun data(): Collection<Array<out Any>> {
                return listOf(
                        arrayOf("http://google.com", "page title"),
                        arrayOf("https://google.com", "page title"),
                        arrayOf("www.google.com", "page title"),
                        arrayOf("https://twitter.com/jdorfman/status/430511497475670016", "page title")
                )
            }
        }

        private lateinit var parser: Parser<List<UrlDescription>>
        @Mock private lateinit var mockTitleFetcher: TitleFetcher

        @Before fun setUp() {
            MockitoAnnotations.initMocks(this)
            parser = Parser.webLinks(mockTitleFetcher)
        }

        @Test fun performTest() {
            Mockito.`when`(mockTitleFetcher.fetchPageTitle(link)).thenReturn(Single.just(pageTitle))
            parser.parse("look at $link for details").test()
                    .assertValue(listOf(UrlDescription(link, pageTitle)))
                    .assertComplete()
        }
    }

    @RunWith(Parameterized::class)
    class WrongLinkTest(private val link: String) {

        companion object {
            @JvmStatic
            @Parameterized.Parameters(name = "shouldn't recognize {0}")
            fun data(): Collection<Array<out Any>> {
                return listOf(
                        arrayOf("http:google.com"),
                        arrayOf("http//google.com"),
                        arrayOf("http:/google.com")
                )
            }
        }

        private lateinit var parser: Parser<List<UrlDescription>>
        @Mock private lateinit var mockTitleFetcher: TitleFetcher

        @Before fun setUp() {
            MockitoAnnotations.initMocks(this)
            parser = Parser.webLinks(mockTitleFetcher)
        }

        @Test fun performTest() {
            parser.parse("look at $link for details").test()
                    .assertValue(emptyList<UrlDescription>())
                    .assertComplete()
        }
    }

    class EdgeCases {
        private lateinit var parser: Parser<List<UrlDescription>>
        @Mock private lateinit var mockTitleFetcher: TitleFetcher
        private val pageTitle = "page title"

        @Before fun setUp() {
            MockitoAnnotations.initMocks(this)
            parser = Parser.webLinks(mockTitleFetcher)
            Mockito.`when`(mockTitleFetcher.fetchPageTitle(Mockito.anyString())).thenReturn(Single.just(pageTitle))
        }

        @Test fun shouldFindLinkInTheStart() {
            val link = "www.google.com"

            parser.parse("$link for details").test()
                    .assertValue(listOf(UrlDescription(link, pageTitle)))
                    .assertComplete()
        }

        @Test fun shouldFindInTheEnd() {
            val link = "www.google.com"
            parser.parse("For details see $link").test()
                    .assertValue(listOf(UrlDescription(link, pageTitle)))
                    .assertComplete()
        }

        @Test fun shouldRecognizeWhenOnlyLink() {
            val link = "www.google.com"
            parser.parse(link).test()
                    .assertValue(listOf(UrlDescription(link, pageTitle)))
                    .assertComplete()
        }

        @Test fun shouldFindMultiple() {
            val linkOne = "www.google.com"
            val linkTwo = "www.yahoo.com"
            parser.parse("follow $linkOne $linkTwo for details").test()
                    .assertValue(listOf(UrlDescription(linkOne, pageTitle), UrlDescription(linkTwo, pageTitle)))
                    .assertComplete()
        }

        @Test fun shouldIgnoreWhenCollapsedWithText() {
            val link = "www.google.com"
            parser.parse("For details$link").test()
                    .assertValue(emptyList<UrlDescription>())
                    .assertComplete()
        }

        @Test fun shouldParseOnNewLine() {
            val link = "www.google.com"
            parser.parse("For details\n$link").test()
                    .assertValue(listOf(UrlDescription(link, pageTitle)))
                    .assertComplete()
        }
    }
}