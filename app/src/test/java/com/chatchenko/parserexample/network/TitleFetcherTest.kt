package com.chatchenko.parserexample.network

import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test

class TitleFetcherTest {
    private var mockWebServer = MockWebServer()
    private lateinit var okHttpClient: OkHttpClient
    private lateinit var fetcher: TitleFetcher
    @Before fun setUp() {
        mockWebServer.start()
        okHttpClient = OkHttpClient.Builder()
                .build()
        fetcher = TitleFetcher(okHttpClient)
    }

    @After fun tearDown() {
        mockWebServer.close()
    }

    @Test fun shouldReturnTitle() {
        val expectedTitle = "Simple page title"
        mockWebServer.enqueue(MockResponse().setResponseCode(200)
                .setHeader("Content-Type", "text/html")
                .setBody("<Html><Head><Title>$expectedTitle</title></head></Html>"))

        fetcher.fetchPageTitle(mockWebServer.url("test.com").toString()).test()
                .assertValue(expectedTitle)
                .assertComplete()
    }
}