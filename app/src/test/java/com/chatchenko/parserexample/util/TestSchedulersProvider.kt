package com.chatchenko.parserexample.util

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

fun testSchedulersProvider(
        ioScheduler: Scheduler = Schedulers.trampoline(),
        mainThread: Scheduler = Schedulers.trampoline()): SchedulersProvider {
    return SchedulersProvider(ioScheduler, mainThread)
}