package com.chatchenko.parserexample.presentation

import com.chatchenko.parserexample.parsers.MessageParser
import com.chatchenko.parserexample.parsers.ParsedMessage
import com.chatchenko.parserexample.util.testSchedulersProvider
import com.google.gson.GsonBuilder
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.MockitoAnnotations

class MessagesPresenterTest {
    private lateinit var presenter: MessagesPresenter
    @Mock private lateinit var mockParser: MessageParser
    @Mock private lateinit var mockView: MessagesPresenter.MessagesView
    private val gson = GsonBuilder().create()

    @Before fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MessagesPresenter(mockParser, testSchedulersProvider(), gson)
        presenter.setView(mockView)
    }

    @Test fun shouldShowMessage() {
        val initialMessage = "test"
        val expectedParsedMessage = ParsedMessage(mentions = listOf("bob"))
        val testSubject = PublishSubject.create<ParsedMessage>()
        `when`(mockParser.parse(initialMessage)).thenReturn(testSubject.singleOrError())

        presenter.parseMessage(initialMessage)
        verify(mockView).showMessage(MessageRepresentation(initialMessage, true))
        verifyNoMoreInteractions(mockView)

        testSubject.onNext(expectedParsedMessage)
        testSubject.onComplete()
        verify(mockView).showMessage(MessageRepresentation(gson.toJson(expectedParsedMessage), false))
    }

    @Test fun shouldShowError() {
        val initialMessage = "test"
        val errorMessage = "error"
        val testSubject = PublishSubject.create<ParsedMessage>()
        `when`(mockParser.parse(initialMessage)).thenReturn(testSubject.singleOrError())

        presenter.parseMessage(initialMessage)
        verify(mockView).showMessage(MessageRepresentation(initialMessage, true))
        verifyNoMoreInteractions(mockView)

        testSubject.onError(Throwable(errorMessage))

        verify(mockView).showError(errorMessage)
    }
}