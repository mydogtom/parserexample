package com.chatchenko.parserexample.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.chatchenko.parserexample.R

class MessagesAdapter : RecyclerView.Adapter<ViewHolder>() {
    private val messagesList: MutableList<MessageRepresentation> = mutableListOf()

    override fun getItemCount(): Int = messagesList.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messagesList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_parsed_message, parent, false)
        return ViewHolder(view)
    }

    fun addMessage(parsedMessage: MessageRepresentation) {
        messagesList.add(parsedMessage)
        notifyItemInserted(messagesList.size - 1)
    }
}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var textView: TextView = itemView.findViewById(R.id.text_view_list_message) as TextView
    private var startSpace: View = itemView.findViewById(R.id.space_start)
    private var endSpace: View = itemView.findViewById(R.id.space_end)

    fun bind(messageRepresentation: MessageRepresentation) {
        textView.text = messageRepresentation.message
        if (messageRepresentation.initial) {
            startSpace.visibility = View.GONE
            endSpace.visibility = View.VISIBLE
        } else {
            startSpace.visibility = View.VISIBLE
            endSpace.visibility = View.GONE
        }
    }
}
