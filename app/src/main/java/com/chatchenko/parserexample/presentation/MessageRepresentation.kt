package com.chatchenko.parserexample.presentation

data class MessageRepresentation(val message: String, val initial: Boolean)