package com.chatchenko.parserexample.presentation

import com.chatchenko.parserexample.parsers.MessageParser
import com.chatchenko.parserexample.util.SchedulersProvider
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MessagesPresenter @Inject constructor(
        private val messageParser: MessageParser,
        private val schedulers: SchedulersProvider,
        private val gson: Gson) {
    private var view: MessagesView? = null
    private val compositeDisposable = CompositeDisposable()

    fun setView(view: MessagesView) {
        this.view = view
    }

    fun destroy() {
        view = null
        compositeDisposable.dispose()
    }

    fun parseMessage(message: String) {
        compositeDisposable.add(
                messageParser.parse(message)
                        .map { MessageRepresentation(gson.toJson(it), false) }
                        .subscribeOn(schedulers.ioScheduler)
                        .observeOn(schedulers.mainThread)
                        .doOnSubscribe { view?.showMessage(MessageRepresentation(message, true)) }
                        .subscribe(
                                { view?.showMessage(it) },
                                {
                                    view?.showError(it.message ?: "Error occured during parsing")
                                    it.printStackTrace()
                                }
                        )
        )
    }

    interface MessagesView {
        fun showMessage(parsedMessage: MessageRepresentation)
        fun showError(errorMessage: String)
    }
}