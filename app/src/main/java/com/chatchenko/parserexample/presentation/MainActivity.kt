package com.chatchenko.parserexample.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.chatchenko.parserexample.ParserApp
import com.chatchenko.parserexample.R
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MessagesPresenter.MessagesView {
    lateinit var messageEditText: EditText
    lateinit var sendButton: Button
    lateinit var recyclerView: RecyclerView
    @Inject lateinit var messagesPresenter: MessagesPresenter
    lateinit var adapter: MessagesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (applicationContext as ParserApp).applicationComponent.inject(this)

        initViews()
        setUpRecyclerView()

        messagesPresenter.setView(this)
        sendButton.setOnClickListener {
            messagesPresenter.parseMessage(messageEditText.text.toString())
            messageEditText.text.clear()
        }
    }

    private fun initViews() {
        messageEditText = findViewById(R.id.edit_text_message) as EditText
        sendButton = findViewById(R.id.button_send) as Button
    }

    private fun setUpRecyclerView() {
        recyclerView = findViewById(R.id.recycler_view_messages) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false).apply {
            stackFromEnd = true
        }
        adapter = MessagesAdapter()
        recyclerView.adapter = adapter
    }

    override fun onDestroy() {
        messagesPresenter.destroy()
        super.onDestroy()
    }

    override fun showMessage(parsedMessage: MessageRepresentation) {
        adapter.addMessage(parsedMessage)
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
    }
}
