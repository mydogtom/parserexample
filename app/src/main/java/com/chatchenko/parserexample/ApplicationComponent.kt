package com.chatchenko.parserexample

import com.chatchenko.parserexample.network.NetworkModule
import com.chatchenko.parserexample.presentation.MainActivity
import com.chatchenko.parserexample.util.UtilsModule
import dagger.Component

@Component(modules = arrayOf(NetworkModule::class, UtilsModule::class))
interface ApplicationComponent {
    fun inject(mainActivity: MainActivity)
}