package com.chatchenko.parserexample.network

import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import javax.inject.Inject

class TitleFetcher @Inject constructor(private val httpClient: OkHttpClient) {
    fun fetchPageTitle(url: String): Single<String> {
        return Single.fromCallable {
            var httpUrl = HttpUrl.parse(url)
            if (httpUrl == null) {
                httpUrl = HttpUrl.Builder()
                        .host(url)
                        .scheme("http")
                        .build()
            }
            val request = Request.Builder()
                    .url(httpUrl)
                    .build()
            val call = httpClient.newCall(request)
            val response: Response = call.execute()
            if (response.code() != 200) {
                response.body()?.close()
                throw Throwable("Non 200 response ${response.code()}")
            }
            val contentType = response.header("Content-Type")
            val subtype = MediaType.parse(contentType)?.subtype()
            if (!subtype.equals("html", ignoreCase = true)) {
                response.body()?.close()
                throw Throwable("Wrong content type $subtype")
            }
            val document: Document = Jsoup.parse(response.body()?.string(), url)
            document.title()
        }
    }
}