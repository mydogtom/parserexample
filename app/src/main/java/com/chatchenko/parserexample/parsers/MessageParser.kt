package com.chatchenko.parserexample.parsers

import android.support.annotation.CheckResult
import com.chatchenko.parserexample.network.TitleFetcher
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class MessageParser @Inject constructor(titleFetcher: TitleFetcher) {

    private val parsers = setOf(
            ParserEntry(Parser.mentions(), { acc, finding -> acc.copy(mentions = finding) }),
            ParserEntry(Parser.emoticons(), { acc, finding -> acc.copy(emoticons = finding) }),
            ParserEntry(Parser.webLinks(titleFetcher), { acc, finding -> acc.copy(links = finding) })
    )

    @CheckResult
    fun parse(message: String): Single<ParsedMessage> {
        return Observable.fromIterable(parsers)
                .flatMapSingle { value -> value.toFindingPair(message) }
                .reduce(ParsedMessage(), { parsedMessage, res -> res.accumulateToParsedMessage(parsedMessage) })
    }
}

private typealias FindingTransformation<T> = (acc: ParsedMessage, finding: T) -> ParsedMessage

private class ParserEntry<T>(val parser: Parser<T>, val transform: FindingTransformation<T>) {
    fun toFindingPair(message: String): Single<FindingPair<T>> {
        return parser.parse(message)
                .map { finding -> FindingPair(finding, transform) }
    }
}

private data class FindingPair<T>(val first: T, val transformation: FindingTransformation<T>) {
    fun accumulateToParsedMessage(parsedMessage: ParsedMessage): ParsedMessage = transformation(parsedMessage, first)
}