package com.chatchenko.parserexample.parsers

import com.chatchenko.parserexample.network.TitleFetcher
import io.reactivex.Observable
import io.reactivex.Single

interface Parser<T> {
    fun parse(message: String): Single<T>

    companion object {
        fun mentions(): Parser<List<String>> = RegExParser("(^| |\n)@([a-zA-Z0-9]+)", { it.groupValues[2] })
        fun emoticons(): Parser<List<String>> = RegExParser("\\(([a-zA-Z0-9]{1,15})\\)", { it.groupValues[1] })
        fun webLinks(titleFetcher: TitleFetcher): Parser<List<UrlDescription>> = WebLinkParser(titleFetcher)
    }
}

private class RegExParser(
        pattern: String,
        private val mapFunction: (result: MatchResult) -> String) : Parser<List<String>> {

    private val regex = Regex(pattern)

    override fun parse(message: String): Single<List<String>> {
        return Single.fromCallable {
            regex.findAll(message)
                    .map { mapFunction(it) }
                    .toList()
        }

    }
}

private class WebLinkParser(private val titleFetcher: TitleFetcher) : Parser<List<UrlDescription>> {
    private val urlLinkParser = RegExParser("(^| |\n)((http://|https://|www\\.)[^ ]+)", { it.groupValues[2] })

    override fun parse(message: String): Single<List<UrlDescription>> {
        return urlLinkParser.parse(message)
                .flatMapObservable { Observable.fromIterable(it) }
                .flatMapSingle { url ->
                    titleFetcher.fetchPageTitle(url)
                            .map { title -> UrlDescription(url, title) }
                }
                .toList()
    }
}