package com.chatchenko.parserexample.parsers

data class ParsedMessage(
        val mentions: List<String> = emptyList(),
        val emoticons: List<String> = emptyList(),
        val links: List<UrlDescription> = emptyList())