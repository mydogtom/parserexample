package com.chatchenko.parserexample.parsers

data class UrlDescription(val url: String, val title: String)