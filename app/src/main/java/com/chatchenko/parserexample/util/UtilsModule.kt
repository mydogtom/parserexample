package com.chatchenko.parserexample.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

@Module
object UtilsModule {
    @JvmStatic
    @Provides
    fun provideSchedulers(): SchedulersProvider = SchedulersProvider()

    @JvmStatic
    @Provides
    fun provideGson(): Gson = GsonBuilder()
            .setPrettyPrinting()
            .create()
}