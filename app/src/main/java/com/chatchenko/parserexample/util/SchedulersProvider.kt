package com.chatchenko.parserexample.util

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

data class SchedulersProvider(
        val ioScheduler: Scheduler = Schedulers.io(),
        val mainThread: Scheduler = AndroidSchedulers.mainThread()
)